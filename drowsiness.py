import cv2 as cv
import dlib
import time
import numpy as np
from imutils import face_utils
from scipy.spatial import distance as dist

import pygame
pygame.mixer.init()
pygame.mixer.music.load("audio/alert.wav")


EYE_ASPECT_RATIO_THRESHOLD=0.3
EYE_ASPECT_RATIO_CONSEC_FRAMES=50
 
COUNTER=0

face_cascade=cv.CascadeClassifier("haarcascades/haarcascade_frontalface_default.xml")
eye_cascade=cv.CascadeClassifier("haarcascades/haarcascade_eye.xml")

def eye_aspect_ratio(eye):
    A = dist.euclidean(eye[1], eye[5])
    B = dist.euclidean(eye[2], eye[4])
    C = dist.euclidean(eye[0], eye[3])
    ear = (A + B) / (2.0 * C)
    return ear

detector=dlib.get_frontal_face_detector()
predictor=dlib.shape_predictor("shape_predictor_68_face_landmarks.dat")

(lStart, lEnd) = face_utils.FACIAL_LANDMARKS_IDXS["left_eye"]
(rStart, rEnd) = face_utils.FACIAL_LANDMARKS_IDXS["right_eye"]

video_capture=cv.VideoCapture(0)
time.sleep(1.0)
while True:
    ret,frame=video_capture.read()
    frame=cv.flip(frame,1)
    gray=cv.cvtColor(frame,cv.COLOR_BGR2GRAY)
    faces=detector(gray,0)
    faces_rectangle=face_cascade.detectMultiScale(gray,1.3,5)
    for (x,y,w,h)in faces_rectangle:
        cv.rectangle(frame,(x,y),(x+w,y+h),(255,0,0),2)
        for face in faces:
            shape=predictor(gray,face)
            shape=face_utils.shape_to_np(shape)
            leftEye=shape[lStart:lEnd]
            rightEye=shape[rStart:rEnd]
            left_eye_aspect_ratio=eye_aspect_ratio(leftEye)
            right_eye_aspect_ratio=eye_aspect_ratio(rightEye)
            eyeaspectratio=(left_eye_aspect_ratio+right_eye_aspect_ratio)/2
            leftEyeHull = cv.convexHull(leftEye)
            rightEyeHull = cv.convexHull(rightEye)
            cv.drawContours(frame, [leftEyeHull], -1, (0, 255, 0), 1)
            cv.drawContours(frame, [rightEyeHull], -1, (0, 255, 0),1)
            if eyeaspectratio<EYE_ASPECT_RATIO_THRESHOLD:
                COUNTER+=1
                if COUNTER>EYE_ASPECT_RATIO_CONSEC_FRAMES:
                    pygame.mixer.music.play(-1)
                    cv.putText(frame,"you are drowsi!",(150,200),cv.FONT_HERSHEY_SIMPLEX,1.5,(0,0,255),2)
            else:
                pygame.mixer.music.stop()
                COUNTER=0
    cv.imshow('video',frame)
    if(cv.waitKey(1)&0xFF==ord('q')):
        break
video_capture.release()
cv.destroyAllWindows()

